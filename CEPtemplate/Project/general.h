#ifndef GENERAL_H
#define GENERAL_H

// 168MHz system frequency
#define SYS_FREQ   ( 168000000 )
// 44,1KHz requested timer frequency
#define TIMER_FREQ (     44100 )

#endif /* GENERAL_H */
