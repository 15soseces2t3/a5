#include <stdint.h>
#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include "CE_Lib.h"
#include "tft.h"
#include "usart.h"

#include "general.h"
#include "pwmHandler.h"

void TIM8_UP_TIM13_IRQHandler (void) {
  if ( TIM8->SR & TIM_SR_UIF ) {
    // TIM8 requested
    TIM8->SR = ~TIM_SR_UIF;

    pwmHandlerTimerIRQHandling ();
  } else {
    // other timer requested?
  }
}

int main (void) {
  PwmOutput pwm = { (SYS_FREQ / TIMER_FREQ) / 2, (SYS_FREQ / TIMER_FREQ) / 8 };

  initCEP_Board ();
  TFT_cursor_off ();

  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;            // enable clock for GPIOA (RM0090 Chap 6.3.10)
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;            // enable clock for GPIOB
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;            // enable clock for GPIOC
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;            // enable clock for GPIOF
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;            // enable clock for GPIOH
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;            // enable clock for GPIOI

  // set IO mode to alternate function (RM0090 Chap 8.3.2)
  GPIOB->MODER   |= (GPIO_Mode_AF 	<< (GPIO_PinSource0 * 2))	| (GPIO_Mode_AF     << (GPIO_PinSource1 * 2));
  GPIOB->OSPEEDR |= (GPIO_Speed_50MHz << (GPIO_PinSource0 * 2)) 	| (GPIO_Speed_50MHz << (GPIO_PinSource1 * 2));
  GPIOB->OTYPER  |= (GPIO_OType_PP 	<< (GPIO_PinSource0))       | (GPIO_OType_PP    << (GPIO_PinSource1));
  GPIOB->PUPDR   |= (GPIO_PuPd_UP 	<< (GPIO_PinSource0 * 2))   | (GPIO_PuPd_UP     << (GPIO_PinSource1 * 2));
  // set alternate function mode for use as pwm output (RM0090 Chap 8.3.2)
  GPIOB->AFR[0]  |= (GPIO_AF_TIM8 	<< (GPIO_PinSource0 * 4))   | (GPIO_AF_TIM8     << (GPIO_PinSource1 * 4));

  pwmHandlerInit ();

  // set priority
  NVIC_SetPriorityGrouping(2);
  // set IRQ handler
  NVIC_SetPriority(TIM8_UP_TIM13_IRQn, 0);
  // enable IRQ
  NVIC_EnableIRQ(TIM8_UP_TIM13_IRQn);

  // fill pwm fifo
  while (!pwmHandlerAddPwmOutputValues (&pwm)) { }

  pwmHandlerStart ();

  // endless
  while (1) {
    if (!pwmHandlerIsFifoFull ()) {
      pwmHandlerAddPwmOutputValues (&pwm);
    }
  }
}
