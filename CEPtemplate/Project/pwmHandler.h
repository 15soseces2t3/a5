#include <stdint.h>

#ifndef PWMHANDLER_H
#define PWMHANDLER_H

#define PWMHANDLER_FIFO_SIZE ( 0x4 )
#define PWMHANDLER_FIFO_INDEX_MASK ( PWMHANDLER_FIFO_SIZE - 1 )

#define PWMHANDLER_TIMER ( TIM8 )
#define PWMHANDLER_PWM1OUT ( PWMHANDLER_TIMER -> CCR3 )
#define PWMHANDLER_PWM2OUT ( PWMHANDLER_TIMER -> CCR2 )

typedef struct PwmOutput {
  uint16_t pwm1;
  uint16_t pwm2;
} PwmOutput;

#endif /* PWMHANDLER_H */

void pwmHandlerTimerIRQHandling (void);

void pwmHandlerInit (void);
void pwmHandlerStart (void);
int pwmHandlerIsFifoFull (void);
int pwmHandlerAddPwmOutputValues (PwmOutput* output);
