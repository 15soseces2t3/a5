#include "pwmHandler.h"

#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <string.h> // memcpy()

#include "general.h"

static volatile PwmOutput pwmHandlerFifo [ PWMHANDLER_FIFO_SIZE ] = { 0 };
static volatile uint16_t pwmHandlerFifoReadIndex = 0;
static volatile uint16_t pwmHandlerFifoWriteIndex = 0;

void pwmHandlerTimerIRQHandling (void) {

  //TODO: Test
  PWMHANDLER_PWM2OUT = (((SYS_FREQ / TIMER_FREQ) / 8) -1);
  PWMHANDLER_PWM1OUT = ((SYS_FREQ / TIMER_FREQ) / 2) -1;

  if (pwmHandlerFifoReadIndex != pwmHandlerFifoWriteIndex) {
    // PWMHANDLER_PWM1OUT = pwmHandlerFifo [pwmHandlerFifoReadIndex].pwm1;
    // PWMHANDLER_PWM2OUT = pwmHandlerFifo [pwmHandlerFifoReadIndex].pwm2;
    //
    // pwmHandlerFifoReadIndex += 1;
    // pwmHandlerFifoReadIndex &= PWMHANDLER_FIFO_INDEX_MASK;
  } else {
    // FIFO is empty!
  }
}

void pwmHandlerInit (void) {
  // Schaefers PWM Demo

  // enable clock for timer 8 (RM0090 Chap 6.3.14)
  RCC->APB2ENR |= RCC_APB2ENR_TIM8EN;

  // ControlRegister1: disabled timer {>RM0090 chap 17.4.1  & stm32f4xx.h)
  PWMHANDLER_TIMER->CR1 = 0;
  // ControlRegister2 {>RM0090 chap 17.4.2  & stm32f4xx.h}
  PWMHANDLER_TIMER->CR2 = 0;
  // PreSCaler: NO prescaler {>RM0090 chap 17.4.11 & stm32f4xx.h}
  PWMHANDLER_TIMER->PSC = 0;
  // AutoReloadRegister: 44100Hz {>RM0090 chap 17.4.12 & stm32f4xx.h}
  PWMHANDLER_TIMER->ARR = (SYS_FREQ / TIMER_FREQ) -1;
  // Dma/InterruptEnableRegister: enable interrupt {>RM0090 chap 17.4.4  & stm32f4xx.h}
  PWMHANDLER_TIMER->DIER = TIM_DIER_UIE;
  // ControlRegister1: auto-reload preload enable {>RM0090 chap 17.4.1  & stm32f4xx.h}
  PWMHANDLER_TIMER->CR1 = TIM_CR1_ARPE;

  // ?
  PWMHANDLER_TIMER->BDTR = TIM_BDTR_MOE;

  // set output compare modes for corresponding IO pins (RM0090 Chap 17.4.7 / 17.4.8)
  PWMHANDLER_TIMER->CCMR1 = TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2PE;
  PWMHANDLER_TIMER->CCMR2 = TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1 | TIM_CCMR2_OC3PE;
  // set complementary output enable for corresponding IO pins (RM0090 Chap 17.4.9)
  PWMHANDLER_TIMER->CCER = TIM_CCER_CC3NE | TIM_CCER_CC2NE;
}

void pwmHandlerStart (void) {
  // (RM0090 Chap 17.3.4)
  PWMHANDLER_TIMER->CR1 |= TIM_CR1_CEN;
}

int pwmHandlerIsFifoFull (void) {
  return pwmHandlerFifoWriteIndex == pwmHandlerFifoReadIndex;
}

// return 1 if full, else 0
int pwmHandlerAddPwmOutputValues (PwmOutput* output) {
  memcpy ((void*) &pwmHandlerFifo [pwmHandlerFifoWriteIndex], output, sizeof (PwmOutput));

  pwmHandlerFifoWriteIndex += 1;
  pwmHandlerFifoWriteIndex &= PWMHANDLER_FIFO_INDEX_MASK;

  return pwmHandlerIsFifoFull ();
}
